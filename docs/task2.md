[Back to Task #1](./task1.md)

### Task #2

```
Given I have called the weather api
When I Submit a list of locations
AND I choose to filter by temperature
Then I should get back a list of locations and weather that match my filters
```


The consumer of the api now requires to filter by a desired temperature, for example they only want to return a location if temperature is above 15° but below 25°. Both filters should work together and seperately. Please implement this filter. 

Once complete, move on to [Task 3](./task3.md)