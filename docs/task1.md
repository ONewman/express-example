[Back to Exercise](./exercise.md)

### Task #1

```
Given I have called the weather api
When I Submit a list of locations
Then I should get back a list of locations and temperature & weatherCondition for each location
```

- The user should be able to submit a list of locations:
```
{
  "locations": ["London", "Canterbury", "Brighton"]
}
```

- The api needs to fetch the current temperature and weather condition from the OpenWeatherAPI for each location and returns them in the API response to the user.
- A list of location names and their corresponding locationIDs can be found [here](../resources/locations.json).


- The user must be presented with an array of locations provided and the temperature & weather condition for each location
Example Response:
```
 [
   {
 		"location": "London",
 		"weather": {
 			"temp": 14,
 			"weatherCondition": "Raining"
 		}
 	},
 	{
 		"location": "Canterbury",
 		"weather": {
 			"temp": 15,
 			"weatherCondition": "Cloudy"
 		}
 	},
 	{
 		"location": "Brighton",
 		"weather": {
 			"temp": 21,
 			"weatherCondition": "Clear"
 		}
 	}
 ]
```

**Some helpful tips for retrieving weather data!**
- To retrieve weather conditions for multiple locations in one call you can perform a get with the following URL:
	- `http://api.openweathermap.org/data/2.5/group?id={locationID1,locationID2}&units=metric&appid={APIKey}`


If you complete this please move on to [Task 2](./task2.md).