[Back to Task 2](./task2.md)

### Task #3

## Overview
To stop the API being used by an unauthorised consumer, the API will also need to support authentication, basic AUTH is fine.


```
Given I have called the weather api
When I Submit a list of locations
AND I have not authenticated
Then I should be prompted to authenticate
```

- The api should only return locations and weather to authenticated users
- basic AUTH is ok


### Notes
- Feel free to install any package you require.

## Finally
-  Please talk through how you would handle caching.