[Back to README](../README.md)

## Exercise Summary

Below you'll find an Epic, along with a link to a Story. Your objective is to deliver the functionality outlined in the Story.

- Write tests where you feel they're required.
- Feel free to google if you need additional information.
- Feel free to install any npm dependencies you need.
- If you're unsure about anything during the exercise then please ask for clarification.

### Epic

```
As a Dunelm customer
I want to find the best location based on weather from a list of submitted locations
So that I can decided where to travel
```

### Some things to think about

Some things we would like you to think about and discuss how you would implement are:
-  How the application handles logging
-  How application handles errors
-  How you would approach testing the application
-  How would you sanitise or validate data?

When you've read through this please move on to [Task 1](./task1.md).
