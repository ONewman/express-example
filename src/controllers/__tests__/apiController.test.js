
const testScenarios = require('./testScenarios.json')
const apiController =  require("../apiController");

describe('Api Controller', () => {
  describe('lookupLocationIds', () => {
    it.each(testScenarios)("Input of '%p'", (input, solution) => {
      expect(apiController.lookupLocationIds(input)).toEqual(solution)
    });
  });
});