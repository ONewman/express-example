#!/usr/bin/env node
const app = require('../src/app');
const http = require('http');

var port = process.env.PORT || '3000';
app.set('port', port);

var server = http.createServer(app);

server.listen(port);